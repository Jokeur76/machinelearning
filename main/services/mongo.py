# mongo1.py
import json
from pymongo import MongoClient


class MongoService:
    def __init__(self, table_name, db_name='tppj_test'):
        self.client = MongoClient('mongodb://localhost:27017/')
        # data base name : 'tppj_test'
        self.db = self.client[db_name]
        self.table = table_name

    def __repr__(self):
        return '<MongoService(name={self.db!r})>'.format(self=self)

    def insert(self, record):
        print('############# record #############')
        print(record)
        self.db[self.table].insert_one(record)
