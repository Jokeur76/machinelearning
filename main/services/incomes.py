from .mongo import MongoService


class IncomeService(MongoService):
    def __init__(self, db_name='tppj_test'):
        super(IncomeService, self).__init__('incomes', db_name)

    def __repr__(self):
        return '<IncomeService(name={self.db!r})>'.format(self=self)

    @staticmethod
    def get_all():
        return []
