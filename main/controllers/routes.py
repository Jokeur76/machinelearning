from flask import Flask, jsonify, request
from main import app
from main.models.expense import Expense, ExpenseSchema
from main.models.income import Income, IncomeSchema
from main.models.transaction_type import TransactionType
from main.services.incomes import IncomeService

transactions = [
    Income('Salary', 5000),
    Income('Dividends', 200),
    Expense('pizza', 50),
    Expense('Rock Concert', 100)
]


@app.route('/incomes')
def get_incomes():
    schema = IncomeSchema(many=True)
    incomes = schema.dump(
        filter(lambda t: t.type == TransactionType.INCOME, transactions)
    )
    return jsonify(incomes.data)


@app.route('/incomes', methods=['POST'])
def add_income():
    print('############# request.get_json() #############')
    print(request.get_json())
    income = IncomeSchema().load(request.get_json()).data
    print('############# income #############')
    print(income)
    # transactions.append(income.data)
    income_service = IncomeService()
    income_service.insert(record=income)
    return "", 204


@app.route('/expenses')
def get_expenses():
    schema = ExpenseSchema(many=True)
    expenses = schema.dump(
        filter(lambda t: t.type == TransactionType.EXPENSE, transactions)
    )
    return jsonify(expenses.data)


@app.route('/expenses', methods=['POST'])
def add_expense():
    expense = ExpenseSchema().load(request.get_json())
    transactions.append(expense.data)
    return "", 204
