import datetime as dt

from marshmallow import Schema, fields


class Transaction:
    def __init__(self, description, amount, transaction_type):
        self.description = description
        self.amount = amount
        self.created_at = dt.datetime.now()
        self.type = transaction_type

    def __repr__(self):
        return '<Transaction(description={self.description!r}, amount={self.amount!r}, ' \
               'created_at={self.created_at!r}, type={self.type!r})>'.format(self=self)


class TransactionSchema(Schema):
    description = fields.Str()
    amount = fields.Number()
    created_at = fields.Date()
    type = fields.Str()
