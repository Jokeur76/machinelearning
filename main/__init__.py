from flask import Flask

app = Flask(__name__)

from main.controllers import routes
